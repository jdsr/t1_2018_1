#include "roteador.h"
#include <stdlib.h>
#include <stdio.h>

uint32_t * roteamento(entrada * rotas, int num_rotas, uint32_t * pacotes, int num_pacotes, entrada * filtros, int num_filtros, int num_enlaces) 
{

	int ndiscarded = 0, temp = num_filtros;
	uint32_t *out_vector, network_table, network_pacotes;
	entrada *routing_table;

	out_vector = calloc(num_enlaces+1, sizeof(uint32_t));

	routing_table = calloc(num_rotas+num_filtros, sizeof(roteamento));

	/*if(out_vector == NULL) {

		printf("Error to allocate the out_vector!\n");
		exit(EXIT_FAILURE);

	}

	if(routing_table == NULL) {

		printf("Error to allocate the priority_row!\n");
		exit(EXIT_FAILURE);

	}

	if((rotas == NULL) | (pacotes == NULL) | (filtros == NULL)) {

		printf("One or more arguments seems to be wrong!\n");
		exit(EXIT_FAILURE);

	}*/

	for(int i = 0; i < num_filtros; ++i)	
		routing_table[i] = filtros[i];

	for(int i = 0; i < num_rotas; ++i, ++num_filtros)
		routing_table[num_filtros] = rotas[i];

	num_filtros = temp;

	for(int i = 0; i < num_pacotes; ++i) {

		ndiscarded = 0;

		for(int j = 0; j < num_rotas+num_filtros; ++j) {

			network_table = routing_table[j].endereco >> (32 - routing_table[j].mascara);
			network_pacotes = pacotes[i] >> (32 - routing_table[j].mascara);

			if((network_pacotes - network_table) == 0) {

				out_vector[(int)routing_table[j].enlace] += 1;
				++ndiscarded;

			}

		}

		if(ndiscarded == 0)
			out_vector[0] += 1;

	}


	return out_vector;

}

