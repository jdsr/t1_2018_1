#include "roteador.h"
#include "simplegrade.h"
#include <assert.h>
#include <time.h>

uint32_t iptoint32(int a, int b, int c, int d)
{
	return (uint32_t) (a<<24)+(b<<16)+(c<<8)+d;
}

void test_1()
{

	uint32_t *out_vector;

	entrada routes[] = {	{iptoint32(19, 8, 1, 3), 24,  1},
							{iptoint32(19, 5, 17, 20), 16,  2},
							{iptoint32(23, 5, 5, 255), 8,  3}};

	entrada filters[] = {	{iptoint32(25, 25, 0, 0) , 24, 0},
							{iptoint32(13, 13, 45, 0), 16, 0},
							{iptoint32(11, 32, 1, 0), 8, 0}};

	uint32_t packages[] = {	iptoint32(19, 8, 0, 3),
							iptoint32(19, 5, 17, 20),
							iptoint32(19, 5, 255, 0),
							iptoint32(23, 0, 0, 0),
							iptoint32(25, 25, 0, 1),
							iptoint32(13, 13, 0, 0),
							iptoint32(11, 6, 6, 6),
							iptoint32(13, 13, 1, 1),
							iptoint32(65, 65, 65, 65)};

	out_vector = roteamento(routes, 3, packages, 9, filters, 3, 3);
	assert(out_vector);

	WHEN("Um pacote para cada rota");

	THEN("O enlace 1 deve receber um pacotes");
	isEqual(out_vector[1], 0, 1);

	THEN("O enlace 2 deve receber dois pacote");
	isEqual(out_vector[2], 2, 1);

	THEN("O enlace 3 deve receber um pacote");
	isEqual(out_vector[3], 1, 1);

	THEN("O firewall deve bloquear 5");
	isEqual(out_vector[0], 6, 1);

	return;
}

void test_2()
{
	time_t t;

	srand((unsigned)time(&t));

	uint32_t *out_vector;

	entrada routes[] = {	{iptoint32(10, 25, 1, 3), 8,  2},
							{iptoint32(5, 4, 35, 1), 16,  1},
							{iptoint32(1, 2, 3, 4), 24,  3}};

	entrada filters[] = {	{iptoint32(25, 25, 0, 0) , 16, 0},
							{iptoint32(13, 13, 45, 0), 24, 0},
							{iptoint32(11, 32, 1, 0)  , 24, 0}};

	uint32_t packages[] = {	iptoint32(13, 13, 45, rand() % 255),
						   	iptoint32(13, 13, 45, rand() % 255),
							iptoint32(25, 25, rand() % 255, rand() % 255),
							iptoint32(11, 32, rand() % 255, rand() % 255),
							iptoint32(13, 13, 45, rand() % 255),
							iptoint32(25, 25, rand() % 255, rand () % 255),
							iptoint32(25, 25, rand() % 255, rand() % 255),
							iptoint32(11, 32, 1, rand() % 255),
							iptoint32(11, 32, 1, rand() % 25)};

	out_vector = roteamento(routes, 3, packages, 9, filters, 3, 4);
	assert(out_vector);

	WHEN("Todos os pacotes devem ser filtrados");

	THEN("Todos os outros enlaces recebem zero pacotes");
	isEqual(out_vector[1], 0, 1);
	isEqual(out_vector[2], 0, 1);
	isEqual(out_vector[3], 0, 1);

	THEN("O firewall deve bloquear um total de nove pacotes");
	isEqual(out_vector[0], 9, 1);

	return;
}

void test_3()
{
	time_t t;

	srand((unsigned)time(&t));

	uint32_t *out_vector;

	entrada routes[] = {	{iptoint32(10, 25, 1, 3), 8,  2},
							{iptoint32(5, 4, 35, 1), 16,  1},
							{iptoint32(1, 2, 3, 4), 24,  3},
							{iptoint32(13, 1, 9, 75), 8, 4},
							{iptoint32(6, 6, 6, 0), 24, 5}};

	entrada filters[] = {	{iptoint32(5, 4, 75, 1), 24, 0}};

	uint32_t packages[] = {	iptoint32(6, 6, 6, rand() % 255),
						   	iptoint32(13, rand() % 255, rand() % 255, rand() % 255),
							iptoint32(1, 2, 3, rand() % 255),
							iptoint32(5, 4, rand() % 255, rand() % 255),
							iptoint32(10, rand() % 255, rand() % 255, rand() % 255)};

	out_vector = roteamento(routes, 5, packages, 5, filters, 1, 6);
	assert(out_vector);

	WHEN("Um pacote para cada enlace");

	THEN("Todos os enlaces recebem um pacote");
	isEqual(out_vector[1], 1, 1);
	isEqual(out_vector[2], 1, 1);
	isEqual(out_vector[3], 1, 1);
	isEqual(out_vector[4], 1, 1);
	isEqual(out_vector[5], 1, 1);

	THEN("O firewall não deve bloquar nenhum pacote");
	isEqual(out_vector[0], 0, 1);

	return;
}

int main()
{

	DESCRIBE("Testes do aluno");

	test_1();
	test_2();
	test_3();
	GRADEME();

	if (grade==maxgrade)
		return 0;
	else return grade;

}