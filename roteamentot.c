#include "roteador.h"
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>

#define INS_NUMTHREADS 1
#define WITH_NUMTHREADS 4
#define BUFFERSIZE 1000

pthread_mutex_t buffer_mutex, result_mutex;

typedef struct thread_struct_argument_body
{

	entrada *routes, *filters;
	int filters_num, enlace_num, packages_num, routes_num;

}thread_struct_argument_t;

pthread_t insert_thread[INS_NUMTHREADS];
pthread_t withdraw_threads[WITH_NUMTHREADS];

int buffer[BUFFERSIZE];
int current_id;
uint32_t *result;

sem_t buffer_full, buffer_empty;

void *insert_buffer(void *arg);
void *withdraw_buffer(void *arg);


uint32_t * roteamento(entrada * rotas, int num_rotas, uint32_t * pacotes, int num_pacotes, entrada * filtros, int num_filtros, int num_enlaces)
{
	
	pthread_mutex_init(&buffer_mutex, NULL);
	sem_init(&buffer_full, 0, BUFFERSIZE);
	sem_init(&buffer_empty, 0, 0);

	thread_struct_argument_t *arguments;

	arguments->routes = rotas;
	arguments->filters = filtros;
	arguments->routes_num = num_rotas;
	arguments->filters_num = num_filtros;
	arguments->enlace_num = num_enlaces;
	arguments->packages_num = num_pacotes;

	for(int i = 0; i < INS_NUMTHREADS; ++i)
		pthread_create(&(insert_thread[i]), NULL, insert_buffer, (void*)pacotes);

	for(int i = 0; i < WITH_NUMTHREADS; ++i)
		pthread_create(&(withdraw_threads[i]), NULL, withdraw_buffer, (void*)arguments);

	for(int i = 0; i < INS_NUMTHREADS; ++i)
		pthread_join(insert_thread[i], NULL);

	for(int i = 0; i < WITH_NUMTHREADS; ++i)
		pthread_join(withdraw_threads[i], NULL);

	return result;

}

void *insert_buffer(void *arg) 
{

	uint32_t *packages; 
	packages = (uint32_t*)arg;

	while(1) {

		sem_wait(&buffer_full);
		pthread_mutex_lock(&buffer_mutex);
		buffer[current_id] = packages[current_id];
		pthread_mutex_unlock(&buffer_mutex);
		sem_post(&buffer_empty);

	}

}


void *withdraw_buffer(void *arguments)
{

	thread_struct_argument_t *arguments_thread;
	arguments_thread = (thread_struct_argument_t*)arguments;
	entrada *routing_table;
	int ndiscarded = 0, temp = arguments_thread->filters_num;
	uint32_t network_pacotes, network_table;

	routing_table = calloc(arguments_thread->routes_num+arguments_thread->filters_num, sizeof(roteamento));

	for(int i = 0; i < arguments_thread->filters_num; ++i)	
		routing_table[i] = arguments_thread->filters[i];

	for(int i = 0; i < arguments_thread->routes_num; ++i, ++arguments_thread->filters_num)
		routing_table[arguments_thread->filters_num] = arguments_thread->routes[i];

	arguments_thread->filters_num = temp;

	for(int i = 0; i < arguments_thread->packages_num; ++i) {
		for(int j = 0; j < arguments_thread->routes_num+arguments_thread->filters_num; ++j) {

			network_table = routing_table[j].endereco >> (32 - routing_table[j].mascara);
			pthread_mutex_lock(&buffer_mutex);
			network_pacotes = buffer[i] >> (32 - routing_table[j].mascara);
			pthread_mutex_unlock(&buffer_mutex);

			if(network_pacotes == network_table) {

				pthread_mutex_lock(&result_mutex);
				result[(int)routing_table[j].enlace] += 1;
				pthread_mutex_unlock(&result_mutex);
				++ndiscarded;

			}

		}

		if(ndiscarded == 0) {

			pthread_mutex_lock(&result_mutex);
			result[0] += 1;
			pthread_mutex_unlock(&result_mutex);
			ndiscarded = 0;

		}

	}

}
